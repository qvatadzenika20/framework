using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using Newtonsoft.Json.Linq;

public class WebDriverManager
{
    private IWebDriver? _driver;

    public IWebDriver GetDriver(string browserType)
    {
        switch (browserType.ToLower())
        {
            case "chrome":
                var chromeOptions = new ChromeOptions();
                chromeOptions.AddArgument("--headless");
                _driver = new ChromeDriver(chromeOptions);
                break;
            default:
                throw new NotSupportedException($"Browser '{browserType}' is not supported.");
        }

        return _driver;
    }

    public void CloseDriver()
    {
        _driver?.Quit();
    }
}

public class GoogleCloudPlatformHomePage(IWebDriver driver)
{
    private readonly IWebDriver _driver = driver;
    private readonly WebDriverWait _wait = new(driver, TimeSpan.FromSeconds(10000));

    public void OpenPage(string url)
    {
        _driver.Navigate().GoToUrl(url);
    }

    public void SearchForPricingCalculator(string query)
    {
        var searchField = _wait.Until(d => d.FindElement(By.XPath("//input[@name='q']")));
        searchField.Click();
        searchField.SendKeys(query);
        searchField.SendKeys(Keys.Enter);
    }
}

public record ComputeEngineConfiguration(int Instances, string OperatingSystem, string ProvisioningModel, string MachineFamily, string Series, string MachineType, bool AddGPUs, string GPUType, int GPUCount, string LocalSSD, string DatacenterLocation, string CommitmentTerm);

public class ScreenshotOnFailure(IWebDriver driver)
{
    private readonly IWebDriver _driver = driver;

    public void CaptureScreenshotOnFailure()
    {
        if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
        {
            var screenshot = ((ITakesScreenshot)_driver).GetScreenshot();
            var filePath = $"screenshots/failure_{DateTime.Now:yyyyMMdd_HHmmss}.png";
            Directory.CreateDirectory("screenshots");
            screenshot.SaveAsFile(filePath, ScreenshotImageFormat.Png);
            Console.WriteLine($"Screenshot saved: {filePath}");
        }
    }
}

[TestFixture]
public class GoogleCloudPricingCalculatorTest
{
    private IWebDriver _driver;

    public GoogleCloudPricingCalculatorTest(IWebDriver driver)
    {
        _driver = driver;
    }

    private WebDriverManager _webDriverManager;
    private GoogleCloudPlatformHomePage _homePage;
    private ScreenshotOnFailure _screenshotOnFailure;
    private JObject _config;

    [SetUp]
    public void Setup()
    {
        _config = JObject.Parse(File.ReadAllText("config.json"));
        var environment = _config["development"];

        _webDriverManager = new WebDriverManager();
        _driver = _webDriverManager.GetDriver(environment["browserType"].ToString());
        _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        _screenshotOnFailure = new ScreenshotOnFailure(_driver);

        _homePage = new GoogleCloudPlatformHomePage(_driver);
    }

    [Test]
    public void TestGoogleCloudPricingCalculator()
    {
        _homePage.OpenPage(_config["development"]["baseUrl"].ToString());

        _homePage.SearchForPricingCalculator("Google Cloud Platform Pricing Calculator");

    }

    [TearDown]
    public void Teardown()
    {
        _screenshotOnFailure.CaptureScreenshotOnFailure();
        _webDriverManager.CloseDriver();
    }
}
